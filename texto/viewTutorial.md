**1+library** 
**2+layout** 
**3+include** 
**4+view** 
**5+pages** 
**6+theme** 
**7+template**
**8+partials**
**9+statics**
**10+components**
En la carpeta *assets* se guardan los ficheros css, js, imágenes, fuentes, videos, etc.
En la carpeta *partials* se guardan partes que compondrán un layout o templates.

##----Schema driven:
To allow us to run both development flows in parallel, we turn to Schema driven development.

##----Archivos de configuración
Cincel utiliza varios archivos de configuraciones. Por lo general, no es necesario tocar estos archivos, a menos que se indique lo contrario.

.babelrc - Archivo de configuración de Babel
.editorconfig- Archivo de configuración EditorConfig para lograr un estilo de codificación consistente
.eslintignore- ESLint ignora el archivo
.eslintrc.yml- Archivo de configuración de ESLint para lograr un estilo de codificación de JavaScript consistente (puede actualizarlo a su preferencia)
.gitattributes- Archivo de configuración de Git para forzar la línea Unix que termina en archivos de texto
.gitignore - Git predeterminado ignora archivos y carpetas
.htmlhintrc- Archivo de configuración HTMLHint
.prettierignore- Más bonito ignorar archivo
.prettierrc- Archivo de configuración más bonito
.stylelintignore- stylelint ignora el archivo
.stylintrc.yml- archivo de configuración stylelint para lograr un estilo de codificación CSS consistente (puede actualizarlo a su preferencia)
.yo-rc.json- Archivo de configuración del generador Yeoman
gulpfile.js - Archivo de configuración de Gulp
index.html - índice del proyecto con las páginas del proyecto enumeradas
package.json - metadatos y dependencias del proyecto
package-lock.json- archivo de bloqueo npm , si usa npm
README.md- Léame del proyecto; puedes usarlo para la documentación del proyecto
webpack.chisel.config.js- archivo de configuración del paquete web
yarn.lock- Archivo de bloqueo de hilo , si usa hilo
