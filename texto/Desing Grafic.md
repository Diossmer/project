# **DISEÑO GRÁFICO Y PUBLICIDAD COMERCIAL**
## *MÉTODO DE BÚSQUEDA:*
~~~~~~
Las preguntas de Cinco W, una H y S: 
~~~~~~
Qué (What): Hace referencia a los acontecimientos, las acciones e ideas que constituyen el tema de la noticia. (Por ejemplo: un accidente de moto con el resultado de dos heridos graves).

Quién/Quiénes (Who): Son los protagonistas, en definitiva todos los personajes que aparecerán en una noticia. (Por ejemplo: un accidente sufrido por dos jóvenes, AML y JPG de 16 y 17 años respectivamente).

Cuándo (When): Sitúa la acción en un tiempo definido. Señalando su inicio, su duración y su final. (Por ejemplo: el accidente tuvo lugar el sábado por la noche en torno a las tres de la madrugada).

Dónde (Where): El espacio, el lugar dónde han ocurrido los hechos. (Por ejemplo: en el kilómetro dieciocho de la autovía A6 a la altura de la ciudad madrileña de Las Rozas).

Por qué (Why): Explica las razones por las que se ha producido el acontecimiento o causa. Incluye cuando es necesario los antecedentes de ese acontecimiento. (Por ejemplo: el conductor de la motocicleta había bebido alcohol según declararon fuentes de la Guardia Civil de la localidad).

Para que (so that): Suele referirse a la finalidad de una acción.(Por ejemplo: hablé alto y claro para que todos pudieran oírme).

Cómo (How): Describe las circunstancias concretas en las que se han producido los hechos. (Por ejemplo: la motocicleta se salió de la calzada en una curva. Los jóvenes se precipitaron a un barranco). Tener en cuenta la Definición, Características y los ejemplos.

**PROGRAMAS A UTILIZAR:**
- INKSCAPE ES IGUAL ADOBE ILLUSTRATOR
- GIMP ES IGUAL PHOTOSHOP
- SCRIBUS ES IGUAL ADOBE INDESIGN
- KRITA ES IGUAL COREL DRAW

CONTENIDO DETALLADO:
----------------------------------
- [ ]CLASE 1

Tema: El Diseño
----------------------------------
- Concepto de Diseño
- Concepto de Diseño Gráfico
- Función
- Elementos
- Áreas
- Elementos básicos del diseñador
- Proceso de diseño
- Adobe Illustrator
- Barra de herramientas
- Elementos básicos de composición
- Creación de formas - El punto
- Exportar documento en Illustrator como jpg

- []CLASE 2

Tema: Elementos básicos de composición
----------------------------------
- La línea
- El plano
- El cuadrado
- El círculo
- El triángulo
- Dibujar triángulos en Illustrator
- Volumen
- Acerca de los rellenos y los trazos
- Controles de relleno y trazo
- Dibujo con Illustrator
- Pinceles
- Dibujo con la herramienta Lápiz
- Dibujo de líneas rectas con la herramienta Segmento de línea

- []CLASE 3

Tema: La Percepción
----------------------------------
- Percepción
- Agrupar y bloquear objetos en Illustrator
- Transformación de objetos en Illustrator
- Leyes de percepción (Gestalt)
- Proximidad
- Similitud o semejanza
- Continuidad
- Ley de cierre o clausura
- Ley de simplicidad
- Figura y fondo
- Ley del contraste
- Ley de Simetría
- Organizar objetos

- []CLASE 4

Tema: El Equilibrio
----------------------------------
- Tipos de percepción
- Elementos importantes dentro de la percepción
- Equilibrio
- Espacio
- Ilusión de profundidad en el espacio
- Ilusión de volumen en el espacio
- Elementos Visuales
- Forma
- Medida
- Color
- Textura

- []CLASE 5

Tema: Composiciones formales
----------------------------------
- Interrelación de Formas
- Composiciones formales
- Traslación
- Rotación
- Reflexión
- Retículas
- La retícula básica


- []CLASE 6

Tema: Composiciones informales
----------------------------------
- Composiciones informales
- Gravedad
- Contraste
- Ritmo
- Centro de interés

- []CLASE 7

Tema: Signos, Símbolos y Señales
----------------------------------
- Signos
- Pictogramas
- Abstracción
- Iconicidad
- Gráficos vectoriales
- Acerca de los trazados
- Dibujo con la herramienta Pluma
- Dibujo de líneas rectas con la herramienta Pluma
- Dibujo de curvas con la herramienta Pluma
- Cómo mover nodos o puntos de un trazado
- Vectorización de imagen
- Calco de ilustraciones

- []CLASE 8

Tema: Teoría del color
----------------------------------
- El lenguaje visual del color
- MEZCLA DE COLORES
- Síntesis aditiva
- Síntesis sustractiva
- ASPECTOS NORMATIVOS DEL COLOR
- Variables Físicas
- Variables Gráficas
- Valor
- Tinte
- Saturación
- Color Luz y pigmento
- Interacciones del color: Armonía y contraste
- Colores complementarios
- Contraste simultáneo
- Contraste de tintes
- Contraste cualitativo
- Contraste claro-oscuro
- Contraste cálidos-fríos
- Contraste de los complementarios
- Contraste cuantitativo

- []CLASE 9

Tema: Tipografía
----------------------------------
- Partes de una letra
- Clasificación tipográfica
- Variables visuales
- Amplitud
- Forma
- Tamaño
- Tono
- Inclinación
- Fuentes y familias tipográficas
- Medidas tipográficas
- Tipografía en Illustrator
- Convertir texto en contornos

- []CLASE 10

Tema: Imagen corporativa
----------------------------------
- Imagen corporativa
- Marca
- Marca verbal
- Marca gráfica
- Los signos identificadores
- Signos fonográficos
- REDISEÑO DE MARCAS

- []CLASE 11

Tema: Imagen corporativa
----------------------------------
- PLAGIO DE MARCAS
- Proceso de Diseño del signo básico de Identidad Visual
- Conceptualización y planteamiento de los atributos
- Caminos creativos
- Brainstorming o lluvia de ideas
- Grilla Constructiva y Pauta Modular

- []CLASE 12

Tema: Papelería institucional
----------------------------------
- Papelería corporativa
- Hoja membretada
- Tarjetas personales
- Carpeta membretada
- Sobre membretado

- []CLASE 13

Tema: Diseño Publicitario
----------------------------------
- Definición de Publicidad
- TIPOS DE PUBLICIDAD (por objetivos generales)
- PUBLICIDAD DE NOTORIEDAD
- PUBLICIDAD INFORMATIVA
- PUBLICIDAD PERSUASIVA O RACIONAL
- PUBLICIDAD SUGESTIVA O EMOCIONAL
- PUBLICIDAD DE RECUERDO
- PUBLICIDAD COMERCIAL
- Funciones de la Publicidad
- OBJETIVOS ESPECÍFICOS DE LA PUBLICIDAD
- Diferencia entre Propaganda y Publicidad
- El Proceso de Comunicación Publicitaria
- Comportamiento del modelo
- Creación de efectos especiales [Illustrator]
- Efectos en Illustrator

- []CLASE 14

Tema: Diseño Publicitario II
----------------------------------
- CONCEPTUALIZACIÓN DE LA AGENCIA DE PUBLICIDAD
- Estructura de las Agencias de Publicidad
- Clasificación de Agencias de Publicidad
- Campaña Publicitaria
- EL MENSAJE
- Ventajas y desventajas de los diferentes medios de comunicación
- Figuras Retóricas en Publicidad
- Clasificación de las figuras retóricas

- []CLASE 15

Tema: Diseño Editorial I
----------------------------------
- DISEÑO EDITORIAL
- La Composición
- Libro
- Formatos de libros
- Sistemas de libros
- Revistas
- Diario

- []CLASE 16

Tema: Diseño Editorial II
----------------------------------
- Volantes (flyers)
- El Folleto
- Brochure institucional
- Catálogo
- Mensajes por correo electrónico
- Mensajes por correo tradicional (mailing)
- El Diseño Técnico
- Infografías

- []CLASE 17

Tema: Adobe Indesign I
----------------------------------
- Introducción al InDesign
- Páginas y pliegos
- Páginas maestras

- []CLASE 18

Tema: Adobe Indesign II
----------------------------------
- Numeración de páginas, capítulos y secciones
- Creación de texto y marcos de texto
- Enlace de texto
- Ceñir texto a objetos

- []CLASE 19

Tema: Señalética
----------------------------------
- LA SEÑALIZACIÓN
- Características de la comunicación señalética
- EL PROGRAMA SEÑALÉTICO
- METODOLOGÍA
- Proceso del programa señalético
- Mensajes señaléticos

- []CLASE 20

Tema: Packaging I
----------------------------------
- Introducción al Packaging
- El envase y sus definiciones
- Clasificación de los envases
- Funciones definidas para los envases
- Consideraciones y requerimientos generales de diseño

- []CLASE 21

Tema: Packaging II
----------------------------------
- El diseño de los envases
- Metodología general para el diseño de un envase
- Procesos para el envasado
- Características de los materiales del envase

- []CLASE 22

Tema: Tecnologías de impresión
----------------------------------
- Proceso de impresión
- El Brief tecnológico
- Impresión directo a Placa (CTP – Computer To Plate)
- Impresión a Escala e Impresión a Demanda
- Medios Cruzados (“cross media”)
- CTF (Computer to film)
- Sistemas de impresión tradicionales
- Flexografía
- Huecograbado (rotograbado)
- Holografía
- Serigrafía

- []CLASE 23

Tema: Merchandising
----------------------------------
- Merchandising
- Planificación de una campaña de Merchandising

- []CLASE 24

Tema: Diseño Web en Illustrator
----------------------------------
- Diseño de sitios web en Illustrator
- Elaboración y objetivos de un proyecto web
- Sectores y mapas de imágenes
- Optimización de imágenes
- Opciones de optimización de gráficos Web

- []CLASE 25

Tema: Photoshop I
----------------------------------
- Introducción a Adobe Photoshop
- Información general sobre el espacio de trabajo
- Introducción a los formatos de imagen
- Introducción a los modos de color
- Cómo convertir una imagen a duotono


- []CLASE 26

Tema: Photoshop II
----------------------------------
- Modificaciones generales de la imagen
- Recorte y enderezamiento de fotografías
- Rotación o volteado de una imagen completa
- Retoque y reparación de imágenes
- Eliminación de ojos rojos
- Selección y máscaras

- []CLASE 27

Photoshop III - Ajustes
----------------------------------
- Colores en Photoshop
- Ajustes tonales y de color - Acerca de los histogramas
- Descripción general de los niveles
- Conversión de una imagen en color en una imagen en blanco y negro
- Equilibrio de Color
- Filtro de Fotografía
- Sustitución del color de los objetos de una imagen
- Mezcla de los canales de color
- Igualar color
- Desaturación de los colores
- Inversión de los colores
- Mejora de los detalles de iluminación y sombra
- Capas de ajuste: una función de ajuste en una capa

- []CLASE 28

Tema: Photoshop IV - Pintura
----------------------------------
- Herramientas de Pintura
- Descripción general del panel Pincel
- Pintura con el pincel Mezclador
- Pintura con el Pincel histórico
- Para instalar un pincel bajado de Internet
- Para guardar una imagen creada por nosotros como pincel

- []CLASE 29

Tema: Photoshop V - Capas
----------------------------------
- Capas en Photoshop
- Los tipos de capas y la edición no destructiva
- Organización de capas
- Acoplar imagen
- Estilos y efectos de capa
- Opciones de estilo de capa

- []CLASE 30

Tema: Photoshop VI - Capas II
----------------------------------
- Máscaras de capas
- Creación de una Máscara de capa
- Opacidad y fusión
- Descripciones de los modos de combinación

- []CLASE 31

Tema: Photoshop VII - Filtros
----------------------------------
- Filtros
- Descripción general de la Galería de filtros
- Referencia de efectos de filtro - Filtros artísticos
- Filtros para desenfocar
- Filtros de trazos de pincel
- Filtros para distorsionar
- Filtros de ruido
- Filtros para pixelizar
- Filtros para interpretar
- Filtros para enfocar
- Filtros para bosquejar
- Filtros para estilizar
- Filtros de textura

- []CLASE 32

Tema: Photoshop VIII
----------------------------------
- Canales
- Los canales de color
- Gestión de trazados
- Edición de trazados
- Conversión entre trazados y bordes de selección
- Conversión de una selección en un trazado
- Corrección de la distorsión y el ruido de la imagen
- Acerca de la distorsión de la lente
- Ventana de Corrección de lente
- Filtro Licuar
- Herramientas de distorsión

- []CLASE 33

Tema: Introducción al Corel
----------------------------------
- Introducción al Corel Draw
- Descripción del espacio de trabajo
- Principales Opciones de la barra de Menú
- Herramientas de precisión
- Caja de herramientas
- Creación de un documento
- Creación de objetos
- Selección, aplicación de tamaño y transformación de objetos
- Aplicación de color y estilo a objetos
- Agrupación y combinación de objetos

- []CLASE 34

Tema: Corel II
----------------------------------
- Elección de colores en Corel
- Relleno uniforme
- Relleno Degradado
- Relleno de Patrón
- Relleno de textura
- Relleno textura postscript
- Relleno interactivo
- Contorno en Corel
- Creación de líneas y curvas
- Herramientas de cuentagotas
- Dar forma a objetos
- Texto en Corel

- []CLASE 35

Tema: Efectos especiales I
----------------------------------
- Herramienta Mezcla
- Silueta
- Distorsionar
- Sombra

- []CLASE 36

Tema: Efectos especiales II
----------------------------------
- Herramienta Envoltura
- Herramienta extrusión interactiva
- Transparencia
- Gráficos avanzados con rellenos de malla
- Creación de páginas

- []CLASE 37

Tema: Mapa de bits, Vectorización
----------------------------------
- Mapa de Bits en Corel
- PowerClip
- Tratamiento de Mapa de bits en Corel
- Efectos en Corel
- Vectorización de Mapa de bits

- []CLASE 38


Tema: Mapa de Bits II
----------------------------------
- Ajuste automático
- Editar mapa de bits
- Laboratorio de ajuste de la imagen
- Enderezar imagen
- Máscara de color de mapa de bits
- Nuevo muestreo
- Convertir una imagen en mapa de bits
- Aplicar filtros a imágenes de mapa de bits

- []CLASE 39

Tema: Marketing
----------------------------------
- Concepto de Marketing
- Mercado
- Mercadotecnia
- Desarrollo del Marketing
- Plan de marketing
- Diseño
- F.O.D.A
- La Empresa
- El Cliente
- Segmentación y posicionamiento
- Producto
- Publicidad
- Promoción

- []CLASE 40

Tema: Campaña publicitaria
----------------------------------
- Campaña Publicitaria
- Procesos de Diseño
- Desarrollo de una campaña Publicitaria
