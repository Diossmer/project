https://www.mclibre.org/consultar/htmlcss/index.html
https://uniwebsidad.com/libros/css?from=librosweb
https://escss.blogspot.com/2016/01/filter-blen-mode-css-juntos.html
https://francescricart.com/tutorial-css/
https://www.juanmacivico87.com/category/css/
https://www.juanmacivico87.com/metodologias-css/
https://aprende-web.net/NT/html5/html5_6.php
https://www.smashingmagazine.com/category/css
https://www.elmaquetadorweb.com/2018/05/jerarquia-css-como-evitar-el-uso-de.html
https://www.w3schools.com/css/css_outline.asp
https://developer.mozilla.org/es/docs/Web/CSS/Especificidad
https://www.w3.org/wiki/CSS_/_Selectores_CSS#Selector_css_simple

*******TEXT
https://developer.mozilla.org/es/docs/Web/CSS/Introducci%C3%B3n/Los:estilos_de_texto
https://developer.mozilla.org/es/docs/Web/CSS/CSS_Fonts
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Text_Decoration
https://developer.mozilla.org/es/docs/Web/CSS/Texto_CSS
******UNIDAD DE MEDIDA REM EM CH EX
*******Display
La propiedad visibility se usa para dos efectos:

* El valor hidden esconde un elemento, pero deja (vacío) el espacio donde debería aparecer.
* El valor collapse esconde filas o columnas de una tabla.

visible
El elemento se muestra normalmente.
hidden
El elemento está escondido, pero los demás elementos se colocan como si ése elemento estuviera presente. Esto funciona como si el elemento fuera absolutamente transparente. Los elementos, con la propiedad visibility: visible, descendientes de ése elemento serán visibles.
collapse
Para tablas, filas, columnas y grupos de tablas. Las filas o columnas se esconden y el espacio que normalmente ocupan, es ocupado (como si aplicáramos el código display: none a las filas/columnas de la tabla). Sin embargo, el tamaño de las columnas o filas restantes, es calculado como si las filas/columnas fusionadas estuvieran presentes. Esta propiedad se diseñó así, al fin de permitir la supresión rápida de filas/columnas de una tabla sin necesidad de calcular de nuevo los altos y los anchos de cada porción de tabla. (Para otros elementos, collapse funciona como hidden).

notas:
El soporte para visibility: collapse no se encuentra o está implementado parcialmente en algunos navegadores modernos. En muchos casos será tratado como visibility: hidden en elementos que no son filas / columnas de una tabla.

visibility:collapse puede cambiar el aspecto de una tabla si hay anidadas tablas dentro de celdas fusionadas, a no ser que se especifique explícitamente visibility: visible en la tabla anidada.

*******position
*******shape-outsite


@reglas


Aspectos Básicos
Selectores
Estilos Básicos
Page Media
Interfáz básica de usuario
Estilos de Color
Estilos de Fuentes
Speech y Ruby
Sintáxis de Atributo Texto y Estilo
2D
3D y animación
